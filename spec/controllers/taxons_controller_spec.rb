require 'rails_helper'

RSpec.describe Potepan::TaxonsController, type: :controller do
  describe "#show" do
    let!(:taxon) { create(:taxon) }

    context '存在するtaxon_idを指定した場合' do
      before do
        get :show, params: { id: taxon.id }
      end

      it 'リクエストが成功する' do
        expect(response).to have_http_status 200
      end

      it '@taxonが期待される値を持つ' do
        expect(assigns(:taxon)).to eq taxon
      end

      it '@taxonsが期待される値を持つ' do
        taxons = Spree::Taxon.all
        expect(assigns(:taxons)).to eq taxons
      end

      it '@productが期待される値を持つ' do
        products = Spree::Product.in_taxon(taxon).includes(master: [:default_price, :images])
        expect(assigns(:products)).to eq products
      end

      it "show templateを呼び出す" do
        expect(response).to render_template :show
      end
    end

    context '存在しないtaxon_idを指定した場合' do
      it 'RecordNotFoundエラーが発生する' do
        expect { get :show, params: { id: 9999 } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
