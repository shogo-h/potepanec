require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe '#show' do
    let!(:product) { create(:product) }

    context '存在するidを指定した場合' do
      before do
        get :show, params: { id: product.id }
      end

      it 'リクエストが成功する' do
        expect(response).to have_http_status 200
      end

      it '@productが期待される値を持つ' do
        expect(assigns(:product)).to eq product
      end

      it "show templateを呼び出す" do
        expect(response).to render_template :show
      end
    end

    context '存在しないidを指定した場合' do
      it 'RecordNotFoundエラーが発生する' do
        expect { get :show, params: { id: 9999 } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    context 'variantが存在しない場合' do
      before do
        get :show, params: { id: product.id }
      end

      it '@variantがproduct.masterの値を持つ' do
        expect(assigns(:variant)).to eq product.master
      end
    end

    context 'variant_idを指定した場合' do
      let!(:variant) { create(:variant, product_id: product.id) }

      before do
        get :show, params: { id: product.id, variant_id: variant.id }
      end

      it '@variantがvariant_idと関連付いた値を持つ' do
        expect(assigns(:variant)).to eq  variant
      end
    end

    context 'variant_idを指定していない場合' do
      let!(:variant) { create(:variant, product_id: product.id) }

      before do
        get :show, params: { id: product.id }
      end

      it '@variantがproduct.variants.firstの値を持つ' do
        expect(assigns(:variant)).to eq product.variants.first
      end
    end

    context '存在しないvariant_idを指定した場合' do
      let!(:variant) { create(:variant, product_id: product.id) }

      it 'RecordNotFoundエラーが発生する' do
        expect { get :show, params: { id: product.id, variant_id: 9999 } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe '@related_products' do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it '@related_productsが期待される値を持つ' do
      expect(assigns(:related_products)).to match_array(related_products)
    end

    it '@related_productsに@productは含まない' do
      expect(assigns(:related_products)).not_to include product
    end

    describe '@related_productsの商品数' do
      context 'productが3つの場合' do
        let!(:related_products) { create_list(:product, 3, taxons: [taxon]) }

        it 'productを3つ持っていること' do
          expect(assigns(:related_products).count).to eq 3
        end
      end

      context 'productが4つの場合' do
        let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

        it 'productを4つ持っていること' do
          expect(assigns(:related_products).count).to eq 4
        end
      end

      context 'productが5つの場合' do
        let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

        it 'productを4つ持っていること' do
          expect(assigns(:related_products).count).to eq 4
        end
      end
    end
  end
end
