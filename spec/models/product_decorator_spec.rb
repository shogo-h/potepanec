require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe 'scope' do
    let!(:taxon) { create(:taxon) }
    let!(:other_taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:other_product) { create(:product, taxons: [other_taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    describe 'search_with_taxon_ids' do
      it 'productが持っているtaxonに関連するproductを取得する' do
        expect(Spree::Product.related_products(product)).to contain_exactly(product, related_product)
        expect(Spree::Product.related_products(other_product)).not_to contain_exactly(product, related_product)
      end
    end

    describe 'except_duplication_product' do
      it '配列からproductを除外する' do
        expect(Spree::Product.except_duplication_products(product)).not_to include product
        expect(Spree::Product.except_duplication_products(product)).to include related_product
      end
    end
  end
end
