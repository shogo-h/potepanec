require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given!(:taxonomy) { create(:taxonomy, name: "Categories") }
  given!(:taxon) { taxonomy.root }
  given!(:shirt_taxon) { create(:taxon, taxonomy: taxonomy, parent: taxon) }
  given!(:shirts) { 3.times.map { |i| create(:product, name: "Shirt #{i}", price: 10 + i, taxons: [shirt_taxon]) } }
  given!(:cap_taxon) { create(:taxon, taxonomy: taxonomy, parent: taxon) }
  given!(:cap) { create(:product, taxons: [cap_taxon]) }

  scenario "商品情報を表示する" do
    visit potepan_product_path(shirts[0])

    expect(page).to have_title "Shirt 0 - BIGBAG Store"
    expect(page).to have_selector '.media-body h2', text: "Shirt 0"
    expect(page).to have_selector '.media-body h3', text: "$10"
    expect(page).to have_selector '.media-body h3 + p', text: "As seen on TV!"
  end

  scenario "関連商品を表示する" do
    visit potepan_product_path(shirts[0])

    expect(page).to have_selector '.productBox h5', text: "Shirt 1"
    expect(page).to have_selector '.productBox h3', text: "$11"
    expect(page).to have_selector '.productBox h5', text: "Shirt 2"
    expect(page).to have_selector '.productBox h3', text: "$12"
  end

  scenario "商品名をクリックすると、その商品のページに遷移する" do
    visit potepan_product_path(shirts[0])

    click_link "Shirt 1"
    expect(current_path).to eq potepan_product_path(shirts[1].id)
  end

  scenario "指定したproductがtaxonsを持たない場合、関連商品を表示しない" do
    visit potepan_product_path(cap)

    expect(page).to have_no_selector '.productsContent h4', text: "関連商品"
  end
end
