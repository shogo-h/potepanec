require 'rails_helper'

RSpec.feature "Taxons", type: :feature do
  given!(:taxonomy) { create(:taxonomy, name: "Categories") }
  given!(:taxon) { taxonomy.root }
  given!(:clothing) { create(:taxon, name: "Clothing", taxonomy: taxonomy, parent: taxon) }
  given!(:t_shirt) { create(:taxon, name: "T-Shirt", taxonomy: taxonomy, parent: clothing) }
  given!(:cap) { create(:taxon, name: "Cap", taxonomy: taxonomy, parent: clothing) }
  given!(:product) { create(:product, name: "Superman T-Shirt", taxons: [t_shirt]) }
  given!(:other_product) { create(:product, name: "Batman Cap", price: "$9999", taxons: [cap]) }

  background do
    visit potepan_category_path(t_shirt.id)
  end

  scenario "カテゴリ一覧・各カテゴリに関連づけられている商品数を表示する" do
    expect(page).to have_selector '.nav a',  text: "Categories"
    expect(page).to have_selector '.nav li', text: "T-Shirt (1)"
    expect(page).to have_selector '.nav li', text: "Cap (1)"
  end

  scenario "指定したtaxon_idからrootまでのパンくずリストを表示する" do
    expect(page).to have_selector '.breadcrumb li', text: "Categories"
    expect(page).to have_selector '.breadcrumb li', text: "Clothing"
    expect(page).to have_selector '.breadcrumb li', text: "T-Shirt"
  end

  scenario "指定したtaxon_idのnameを表示する" do
    expect(page).to have_title "T-Shirt - BIGBAG Store"
    expect(page).to have_selector 'h2', text: "T-Shirt"
  end

  scenario "指定したtaxon_idに関連する商品の情報を表示する" do
    expect(page).to have_selector 'h5', text: "Superman T-Shirt"
    expect(page).to have_selector 'h3', text: "$19.99"
  end

  scenario "指定したtaxon_idと関連しない商品の情報は表示されない" do
    expect(page).to have_no_selector 'h5', text: "Batman Cap"
    expect(page).to have_no_selector 'h3', text: "$9999"
  end

  scenario "商品名をクリックすると、その商品のページに遷移する" do
    click_link "Superman T-Shirt"
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
