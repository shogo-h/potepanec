class Potepan::TaxonsController < ApplicationController
  def show
    @taxons = Spree::Taxon.all
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.in_taxon(@taxon).includes_price_images
  end
end
