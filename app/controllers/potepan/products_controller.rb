class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.includes(product_properties: :property).friendly.find(params[:id])
    if @product.variants.any?
      @variant = params[:variant_id] ? @product.variants.find(params[:variant_id]) : @product.variants.first
    else
      @variant = @product.master
    end
    @related_products = Spree::Product.
      includes_price_images.
      related_products(@product).
      except_duplication_products(@product).
      sample(4)
  end
end
