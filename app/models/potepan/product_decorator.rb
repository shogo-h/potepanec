Spree::Product.class_eval do
  scope :includes_price_images, -> { includes(master: [:default_price, :images]) }
  scope :related_products, -> (current_product) do
    joins(:taxons).where(spree_taxons: { id: current_product.taxons.ids })
  end
  scope :except_duplication_products, -> (current_product) do
    where.not(id: current_product.id).distinct
  end
end
