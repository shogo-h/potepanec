module ProductsHelper
  def product_and_variant_images(product, variant)
    product_images = product.images.to_a
    product_images += variant.images unless variant.is_master
    product_images
  end

  def show_main_image(image, i)
    content_tag(:div, class: "item #{'active' if i == 0}", data: { thumb: i }) do
      image_tag(image.attachment.url(:large))
    end
  end

  def show_thum_image(image, i)
    content_tag(:div, class: 'thumb', data: { target: '#carousel', slide_to: i }) do
      image_tag(image.attachment.url(:small))
    end
  end
end
