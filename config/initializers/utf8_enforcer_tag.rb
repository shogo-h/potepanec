#商品詳細ページでvariantを変更した際に表示されるutfパラメータを消す
module ActionView
  module Helpers
    module FormTagHelper
      def utf8_enforcer_tag
        "".html_safe
      end
    end
  end
end
